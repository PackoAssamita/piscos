#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Piscos is a system information for psychologists.

(C) Copyright 2003-2014 Francisco Jose Moreno Llorca <packo@assamita.net>

Piscos is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Piscos is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Piscos.  If not, see <http://www.gnu.org/licenses/>.
"""
#Boa:PopupWindow:wxPopupWindow1

from wxPython.wx import *

def create(parent):
    return wxPopupWindow1(parent)

[wxID_WXPOPUPWINDOW1, wxID_WXPOPUPWINDOW1BUTTON1, wxID_WXPOPUPWINDOW1BUTTON2, 
 wxID_WXPOPUPWINDOW1STATICTEXT1, wxID_WXPOPUPWINDOW1STATICTEXT2, 
 wxID_WXPOPUPWINDOW1STATICTEXT3, 
] = map(lambda _init_ctrls: wxNewId(), range(6))

class wxPopupWindow1(wxPopupWindow):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wxPopupWindow.__init__(self, flags=wxSIMPLE_BORDER, parent=prnt)
        self.SetPosition(wxPoint(497, 318))
        self.SetSize(wxSize(386, 164))
        self.SetToolTipString('Attention!')

        self.staticText1 = wxStaticText(id=wxID_WXPOPUPWINDOW1STATICTEXT1,
              label='Attention! you are going to delete', name='staticText1',
              parent=self, pos=wxPoint(72, 24), size=wxSize(296, 20), style=0)

        self.staticText2 = wxStaticText(id=wxID_WXPOPUPWINDOW1STATICTEXT2,
              label='the user information', name='staticText2',
              parent=self, pos=wxPoint(80, 48), size=wxSize(225, 24), style=0)

        self.staticText3 = wxStaticText(id=wxID_WXPOPUPWINDOW1STATICTEXT3,
              label='OK?', name='staticText3', parent=self,
              pos=wxPoint(72, 88), size=wxSize(71, 20), style=0)

        self.button1 = wxButton(id=wxID_WXPOPUPWINDOW1BUTTON1, label='Yes',
              name='button1', parent=self, pos=wxPoint(160, 88), size=wxSize(80,
              34), style=0)
        self.button1.SetBackgroundColour(wxColour(0, 255, 0))
        self.button1.SetToolTipString('Are you sure?')
        EVT_BUTTON(self.button1, wxID_WXPOPUPWINDOW1BUTTON1, self.OnButtonSi)

        self.button2 = wxButton(id=wxID_WXPOPUPWINDOW1BUTTON2, label='No',
              name='button2', parent=self, pos=wxPoint(264, 88), size=wxSize(80,
              34), style=0)
        self.button2.SetBackgroundColour(wxColour(255, 0, 0))
        self.button2.SetToolTipString('Right!')
        EVT_BUTTON(self.button2, wxID_WXPOPUPWINDOW1BUTTON2, self.OnButtonNo)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.padre = parent

    def OnButtonSi(self, event):
        self.padre.padre.a.elimina_paciente(self.padre.padre.a.datos['codigo'])   
        try:
            self.padre.textos.borrar_paciente(self.padre.padre.a.datos['codigo'])
        except:
            print "Personal information not deleted"
        self.padre.padre.textCtrl1.SetValue("")
        self.padre.padre.listau = []
        self.padre.padre.listau = self.padre.padre.a.lista_nombres()
        self.padre.padre.comboBox1.Clear()
        self.padre.padre.comboBox1.Append('New')
        for aux in self.padre.padre.listau:
            self.padre.padre.comboBox1.Append(aux)
        self.padre.padre.Show()
        self.padre.Destroy()
        self.Destroy()

    def OnButtonNo(self, event):
        self.Destroy()

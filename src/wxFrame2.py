#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Piscos is a system information for psychologists.

(C) Copyright 2003-2014 Francisco Jose Moreno Llorca <packo@assamita.net>

Piscos is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Piscos is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Piscos.  If not, see <http://www.gnu.org/licenses/>.
"""
#Boa:Frame:wxFrame2
#Formulario principal del paciente

from wxPython.wx import *
from wxPython.stc import *
import wxFrame4
from psico import datapsico
import time

def create(parent):
    return wxFrame2(parent)

[wxID_WXFRAME2, wxID_WXFRAME2BORRAR_FICHA, wxID_WXFRAME2BUTTON1, 
 wxID_WXFRAME2BUTTON2, wxID_WXFRAME2BUTTON3, wxID_WXFRAME2BUTTON4, 
 wxID_WXFRAME2NOTEBOOK1, wxID_WXFRAME2STATICTEXT1, wxID_WXFRAME2STATICTEXT2, 
 wxID_WXFRAME2STYLEDTEXTCTRL1, wxID_WXFRAME2TEXTCTRL1, wxID_WXFRAME2TEXTCTRL2, 
 wxID_WXFRAME2TEXTCTRL3, wxID_WXFRAME2TEXTOPROGRA, 
] = map(lambda _init_ctrls: wxNewId(), range(14))

class wxFrame2(wxFrame):
    def _init_coll_notebook1_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.styledTextCtrl1, select=True,
              text='Notes')
        parent.AddPage(imageId=-1, page=self.TextoProgra, select=False,
              text='Roadmap')

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wxFrame.__init__(self, id=wxID_WXFRAME2, name='wxFrame2', parent=prnt,
              pos=wxPoint(359, 142), size=wxSize(838, 513),
              style=wxDEFAULT_FRAME_STYLE, title="Error")
        self.SetClientSize(wxSize(830, 479))
        EVT_CLOSE(self, self.OnBotonGuardarSalir1)

        self.textCtrl1 = wxTextCtrl(id=wxID_WXFRAME2TEXTCTRL1, name='textCtrl1',
              parent=self, pos=wxPoint(120, 32), size=wxSize(104, 21), style=0,
              value='')
        self.textCtrl1.SetToolTipString('Edit in "Personal Inf"')

        self.staticText1 = wxStaticText(id=wxID_WXFRAME2STATICTEXT1,
              label='Name', name='staticText1', parent=self, pos=wxPoint(48,
              40), size=wxSize(64, 16), style=0)
        self.staticText1.SetToolTipString('Given Name')

        self.staticText2 = wxStaticText(id=wxID_WXFRAME2STATICTEXT2,
              label='Phone Ns.', name='staticText2', parent=self,
              pos=wxPoint(264, 40), size=wxSize(64, 16), style=0)

        self.textCtrl2 = wxTextCtrl(id=wxID_WXFRAME2TEXTCTRL2, name='textCtrl2',
              parent=self, pos=wxPoint(352, 32), size=wxSize(80, 21), style=0,
              value='')
        self.textCtrl2.SetToolTipString('Edit in "Personal Inf"')

        self.textCtrl3 = wxTextCtrl(id=wxID_WXFRAME2TEXTCTRL3, name='textCtrl3',
              parent=self, pos=wxPoint(352, 64), size=wxSize(80, 21), style=0,
              value='')
        self.textCtrl3.SetToolTipString('Edit in "Personal Inf"')

        self.button1 = wxButton(id=wxID_WXFRAME2BUTTON1,
              label='Add date', name='button1', parent=self,
              pos=wxPoint(80, 96), size=wxSize(136, 24), style=0)
        self.button1.SetToolTipString('Add the date to the note')
        EVT_BUTTON(self.button1, wxID_WXFRAME2BUTTON1, self.OnBotonAnadir1)

        self.button2 = wxButton(id=wxID_WXFRAME2BUTTON2,
              label='Save & exit', name='button2', parent=self,
              pos=wxPoint(600, 32), size=wxSize(112, 24), style=0)
        self.button2.SetToolTipString('Save information and back to initial window')
        EVT_BUTTON(self.button2, wxID_WXFRAME2BUTTON2,
              self.OnBotonGuardarSalir1)

        self.button3 = wxButton(id=wxID_WXFRAME2BUTTON3, label='Scheduler',
              name='button3', parent=self, pos=wxPoint(464, 32),
              size=wxSize(120, 24), style=0)
        self.button3.SetToolTipString('Open Scheduler')
        EVT_BUTTON(self.button3, wxID_WXFRAME2BUTTON3, self.OnBotonAgenda1)

        self.button4 = wxButton(id=wxID_WXFRAME2BUTTON4, label='Personal Inf',
              name='button4', parent=self, pos=wxPoint(464, 64),
              size=wxSize(120, 24), style=0)
        self.button4.SetToolTipString('View/Modify personal information')
        EVT_BUTTON(self.button4, wxID_WXFRAME2BUTTON4, self.OnBotonFicha1)

        self.notebook1 = wxNotebook(id=wxID_WXFRAME2NOTEBOOK1, name='notebook1',
              parent=self, pos=wxPoint(32, 128), size=wxSize(768, 312),
              style=0)
        EVT_NOTEBOOK_PAGE_CHANGED(self.notebook1, wxID_WXFRAME2NOTEBOOK1,
              self.OnNotebook1NotebookPageChanged)

        self.TextoProgra = wxStyledTextCtrl(id=wxID_WXFRAME2TEXTOPROGRA,
              name='TextoProgra', parent=self.notebook1, pos=wxPoint(0, 0),
              size=wxSize(760, 286), style=0)
        self.TextoProgra.SetUseHorizontalScrollBar(False)
        self.TextoProgra.SetScrollWidth(500)
        self.TextoProgra.SetToolTipString('Make a sessions roadmap')

        self.styledTextCtrl1 = wxStyledTextCtrl(id=wxID_WXFRAME2STYLEDTEXTCTRL1,
              name='styledTextCtrl1', parent=self.notebook1, pos=wxPoint(0, 0),
              size=wxSize(760, 286), style=0)
        self.styledTextCtrl1.SetUseHorizontalScrollBar(False)
        self.styledTextCtrl1.SetScrollWidth(20)
        self.styledTextCtrl1.SetTargetEnd(0)
        self.styledTextCtrl1.SetOvertype(True)
        self.styledTextCtrl1.SetToolTipString('Write notes')

        self.borrar_ficha = wxButton(id=wxID_WXFRAME2BORRAR_FICHA,
              label='Delete user', name='borrar_ficha', parent=self,
              pos=wxPoint(680, 448), size=wxSize(104, 24), style=0)
        self.borrar_ficha.SetBackgroundColour(wxColour(128, 128, 128))
        self.borrar_ficha.SetToolTipString('Delete the current user, no back')
        EVT_BUTTON(self.borrar_ficha, wxID_WXFRAME2BORRAR_FICHA,
              self.on_borrar_ficha)

        self._init_coll_notebook1_Pages(self.notebook1)

    def __init__(self, parent):
        self.pagina_activa = 'notas'
        self._init_ctrls(parent)
        
        
     
    def OnBotonAnadir1(self, event):
        tiempo = time.localtime()
        #for aux in time.asctime():
        dia = time.strftime("%A",tiempo)
               
        dia_mes = time.strftime("%d",tiempo)
        mes = time.strftime("%B",tiempo)
        
        fecha = "\n"+dia+","+dia_mes + " de " + mes
        if self.pagina_activa == 'sesiones':
            self.TextoProgra.AddText(fecha)
        elif self.pagina_activa == 'notas':
            self.styledTextCtrl1.AddText(fecha)

    def OnBotonAgenda1(self, event):
        self.Hide()
        self.agenda = wxFrame4.create(None)
        self.agenda.padre = self
        #self.agenda.paciente = usuario
        # needed when running from Boa under Windows 9X
        #self.SetTopWindow(self.paciente)
        self.agenda.Show()#;self.usuario.Hide();self.usuario.Show()

    def OnBotonFicha1(self, event):
        self.Hide()
        import wxFrame3
        self.ficha = wxFrame3.create(None)
        self.ficha.padre = self
        self.ficha.datos = self.padre.a.datos
        self.ficha.mostrar_elementos()
        titulo = self.GetTitle()
        titulo = 'Personal Infor. of ' + titulo
        self.ficha.SetTitle( titulo)
        self.ficha.Show()
     
    def OnBotonGuardarSalir1(self, event):
        if self.padre.a.datos['nombre']=='New':
            self.on_borrar_ficha(event)
        else:            
            self.padre.textCtrl1.SetValue("")
            self.padre.listau = []
            self.padre.listau = self.padre.a.lista_nombres()
            self.padre.comboBox1.Clear()
            self.padre.comboBox1.Append('New')
            for aux in self.padre.listau:
                self.padre.comboBox1.Append(aux)
        self.textos.guardar_paciente_notas(self.padre.a.datos['codigo'])
        self.textos.guardar_paciente_sesiones(self.padre.a.datos['codigo'])
        self.padre.Show()
        self.Destroy()

    def OnNotebook1NotebookPageChanged(self, event):
        if event.GetSelection() == 1:
            self.pagina_activa = 'sesiones'
        elif event.GetSelection() == 0:
            self.pagina_activa = 'notas'
        else:
            print "Error changing active panel on notebook1"

    def on_borrar_ficha(self, event):
        import wxPopupWindow1
        self.popup = wxPopupWindow1.create(self)
        self.popup.Show()
        
    

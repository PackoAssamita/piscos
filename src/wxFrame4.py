#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Piscos is a system information for psychologists.

(C) Copyright 2003-2014 Francisco Jose Moreno Llorca <packo@assamita.net>

Piscos is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Piscos is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Piscos.  If not, see <http://www.gnu.org/licenses/>.
"""
#Boa:Frame:wxFrame4
#Calendario

from wxPython.wx import *
from wxPython.grid import *

def create(parent):
    return wxFrame4(parent)

[wxID_WXFRAME4, wxID_WXFRAME4GRID1, 
] = map(lambda _init_ctrls: wxNewId(), range(2))

class wxFrame4(wxFrame):
    def _init_utils(self):
        # generated method, don't edit
        pass

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wxFrame.__init__(self, id=wxID_WXFRAME4, name='', parent=prnt,
              pos=wxPoint(357, 210), size=wxSize(853, 467),
              style=wxDEFAULT_FRAME_STYLE, title='Scheculer')
        self._init_utils()
        self.SetClientSize(wxSize(853, 467))
        EVT_CLOSE(self, self.OnBotonSalir)

        self.grid1 = wxGrid(id=wxID_WXFRAME4GRID1, name='grid1', parent=self,
              pos=wxPoint(0, 0), size=wxSize(853, 467), style=0)
        self.grid1.SetDefaultCellOverflow(1)
        self.grid1.SetCellHighlightPenWidth(5)
        self.grid1.SetCellHighlightROPenWidth(3)
        self.grid1.SetDefaultColSize(90)
        self.grid1.SetDefaultRowSize(40)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.grid1.CreateGrid(20,7)
        self.grid1.SetColLabelValue(0,"Monday")
        self.grid1.SetColLabelValue(1,"Tuesday")
        self.grid1.SetColLabelValue(2,"Wednesday")
        self.grid1.SetColLabelValue(3,"Thursday")
        self.grid1.SetColLabelValue(4,"Friday")
        self.grid1.SetColLabelValue(5,"Saturday")
        self.grid1.SetColLabelValue(6,"Sunday")

    def OnBotonSalir(self, event):
        self.padre.Show()
        self.Destroy()
        

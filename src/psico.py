#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Piscos is a system information for psychologists.

(C) Copyright 2003-2014 Francisco Jose Moreno Llorca <packo@assamita.net>

Piscos is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Piscos is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Piscos.  If not, see <http://www.gnu.org/licenses/>.
"""
#Modulo para el manejo de los archivos de guardado de informacion clinica
import bz2
import os

class datapsico:
    
    def __init__(self,padre):
        print "Loading Piscos"
        self.padre = padre
    def borrar_paciente(self,codigo):
        try:
            os.remove(codigo + ".pan")
            os.remove(codigo + ".pas")
            os.remove(codigo + ".pad")
        except:
            print "Removed used before login"
        
    #lo segundo despues de inicializar a de ser pasarle el frame
    def abrir_paciente_notas(self,codigo):
        print "Opening notes"
        archivo = codigo + '.pan'
        print "   file " + archivo
        try:
            self.file1 = bz2.BZ2File(archivo,'r')
            print "...found and opened"
        except:
            print "not found, creating a new one"
            return -1
        while (1):
            linea = self.file1.readline()
            print "   -line found: "+linea
            if linea == "":
                break
            self.padre.usuario.styledTextCtrl1.AddText(linea)
        self.file1.close()
    def abrir_paciente_sesiones(self,codigo):
        print "Opening sessions"
        archivo = codigo + '.pas'
        try:
            self.file2 = bz2.BZ2File(archivo,'r')
        except:
            return -1
        while (1):
            linea = self.file2.readline()
            if linea == "":
                break
            self.padre.usuario.TextoProgra.AddText(linea)
        self.file2.close()
    def abrir_paciente_datos(self,codigo):
        print "Opening clinic information"
        archivo = codigo + '.pad'
        try:
            self.file3 = bz2.BZ2File(archivo,'r')
        except:
            return -1
        self.file3.close()

    def guardar_paciente_notas(self,codigo):
        print "Saving notes"
        archivo = codigo + '.pan'
        self.file1 = bz2.BZ2File(archivo,'w')
        self.file1.write(self.padre.usuario.styledTextCtrl1.GetText())
        self.file1.close()
        
    def guardar_paciente_sesiones(self,codigo):
        print "Saving sessions"
        archivo = codigo + '.pas'
        self.file2 = bz2.BZ2File(archivo,'w')
        self.file2.write(self.padre.usuario.TextoProgra.GetText())
        self.file2.close()
        
    def guardar_paciente_datos(self,codigo):
        print "Saving clinic information"
        archivo = codigo + '.pad'
        self.file3 = bz2.BZ2File(archivo,'w')
        self.file3.close()
        

#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Piscos is a system information for psychologists.

(C) Copyright 2003-2014 Francisco Jose Moreno Llorca <packo@assamita.net>

Piscos is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Piscos is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Piscos.  If not, see <http://www.gnu.org/licenses/>.
"""
#Boa:MiniFrame:wxMiniFrame1

from wxPython.wx import *
import string

#listau = []

def create(parent):
    return wxMiniFrame1(parent)

[wxID_WXMINIFRAME1, wxID_WXMINIFRAME1BITMAPBUTTON1, 
 wxID_WXMINIFRAME1BITMAPBUTTON2, wxID_WXMINIFRAME1BUTTON1, 
 wxID_WXMINIFRAME1COMBOBOX1, wxID_WXMINIFRAME1STATICTEXT1, 
 wxID_WXMINIFRAME1STATICTEXT2, wxID_WXMINIFRAME1STATICTEXT3, 
 wxID_WXMINIFRAME1TEXTCTRL1, wxID_WXMINIFRAME1TEXTCTRL2, 
] = map(lambda _init_ctrls: wxNewId(), range(10))

class wxMiniFrame1(wxMiniFrame):
    listau = []
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wxMiniFrame.__init__(self, id=wxID_WXMINIFRAME1, name='', parent=prnt,
              pos=wxPoint(560, 350), size=wxSize(355, 126),
              style=wxDEFAULT_FRAME_STYLE, title='wxMiniFrame1')
        self.SetClientSize(wxSize(355, 126))

        self.staticText1 = wxStaticText(id=wxID_WXMINIFRAME1STATICTEXT1,
              label='Add :', name='staticText1', parent=self,
              pos=wxPoint(16, 16), size=wxSize(44, 14), style=0)

        self.textCtrl1 = wxTextCtrl(id=wxID_WXMINIFRAME1TEXTCTRL1,
              name='textCtrl1', parent=self, pos=wxPoint(72, 8),
              size=wxSize(120, 24), style=wxTE_PROCESS_ENTER, value='')
        self.textCtrl1.SetToolTipString('Name')
        EVT_TEXT_ENTER(self.textCtrl1, wxID_WXMINIFRAME1TEXTCTRL1,
              self.OnAnadirEnter)

        self.staticText2 = wxStaticText(id=wxID_WXMINIFRAME1STATICTEXT2,
              label='age ', name='staticText2', parent=self,
              pos=wxPoint(208, 16), size=wxSize(50, 14), style=0)

        self.textCtrl2 = wxTextCtrl(id=wxID_WXMINIFRAME1TEXTCTRL2,
              name='textCtrl2', parent=self, pos=wxPoint(272, 8),
              size=wxSize(24, 22), style=wxTE_PROCESS_ENTER, value='')
        EVT_TEXT_ENTER(self.textCtrl2, wxID_WXMINIFRAME1TEXTCTRL2,
              self.OnEdadEnter)

        self.bitmapButton1 = wxBitmapButton(bitmap=wxNullBitmap,
              id=wxID_WXMINIFRAME1BITMAPBUTTON1, name='bitmapButton1',
              parent=self, pos=wxPoint(312, 8), size=wxSize(32, 24),
              style=wxBU_AUTODRAW, validator=wxDefaultValidator)
        self.bitmapButton1.SetBitmapDisabled(wxBitmap('button_ok.png',
              wxBITMAP_TYPE_PNG))
        self.bitmapButton1.SetBitmapFocus(wxBitmap('button_ok.png',
              wxBITMAP_TYPE_PNG))
        self.bitmapButton1.SetBitmapLabel(wxBitmap('button_ok.png',
              wxBITMAP_TYPE_PNG))
        self.bitmapButton1.SetBitmapSelected(wxBitmap('button_ok.png',
              wxBITMAP_TYPE_PNG))
        self.bitmapButton1.SetToolTipString('Para aceptar la entrada')
        EVT_BUTTON(self.bitmapButton1, wxID_WXMINIFRAME1BITMAPBUTTON1,
              self.boton_anadir)

        self.staticText3 = wxStaticText(id=wxID_WXMINIFRAME1STATICTEXT3,
              label='Added: ', name='staticText3', parent=self,
              pos=wxPoint(8, 48), size=wxSize(77, 14), style=0)

        self.bitmapButton2 = wxBitmapButton(bitmap=wxNullBitmap,
              id=wxID_WXMINIFRAME1BITMAPBUTTON2, name='bitmapButton2',
              parent=self, pos=wxPoint(312, 40), size=wxSize(32, 26),
              style=wxBU_AUTODRAW, validator=wxDefaultValidator)
        self.bitmapButton2.SetBitmapDisabled(wxBitmap('edittrash.png',
              wxBITMAP_TYPE_PNG))
        self.bitmapButton2.SetBitmapFocus(wxBitmap('edittrash.png',
              wxBITMAP_TYPE_PNG))
        self.bitmapButton2.SetBitmapLabel(wxBitmap('edittrash.png',
              wxBITMAP_TYPE_PNG))
        self.bitmapButton2.SetBitmapSelected(wxBitmap('edittrash.png',
              wxBITMAP_TYPE_PNG))
        EVT_LEFT_DOWN(self.bitmapButton2, self.on_borrar)

        self.button1 = wxButton(id=wxID_WXMINIFRAME1BUTTON1, label='OK',
              name='button1', parent=self, pos=wxPoint(136, 88),
              size=wxSize(104, 24), style=0)
        EVT_BUTTON(self.button1, wxID_WXMINIFRAME1BUTTON1, self.OnButton1Button)

        self.comboBox1 = wxComboBox(choices=[], id=wxID_WXMINIFRAME1COMBOBOX1,
              name='comboBox1', parent=self, pos=wxPoint(88, 40),
              size=wxSize(208, 25), style=0, validator=wxDefaultValidator,
              value='')
        self.comboBox1.SetLabel('')

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.parent = parent
        if parent.hijo == '1':
            for aux in parent.padre.padre.a.lista_hijos():
                self.comboBox1.Append(aux)
        if parent.hijo == '0':
            for aux in parent.padre.padre.a.lista_hermanos():
                self.comboBox1.Append(aux)

    def boton_anadir(self, event):
        if self.textCtrl1.GetValue() != "":
            if self.parent.hijo == '1':
                tipo = '1'
            else:
                tipo = '0'
            if self.conexion.datos['hijos'] == 'No' and self.conexion.datos['hermanos'] == 'No':
                self.conexion.crear('hermanos')
            self.conexion.datos[self.GetTitle()] = 'Si'   
            self.conexion.hermano['nombre'] = self.textCtrl1.GetValue()
            self.conexion.hermano['edad'] = self.textCtrl2.GetValue()
            self.conexion.hermano['tipo'] = tipo
            self.conexion.nuevo_hermano()
            self.refrescar()
        
    def OnButton1Button(self, event):
        self.parent.mostrar_elementos()
        self.Destroy()

    def on_borrar(self, event):
        nombre_sepa = self.comboBox1.GetValue().split()
        self.conexion.hermano['nombre'] = nombre_sepa[0]
        if self.conexion.hermano['nombre'] != "" :
            self.conexion.eliminar_hermano()
            self.refrescar()
    def refrescar(self):
        self.comboBox1.Clear()
        if self.parent.hijo == '0':
            for aux in self.parent.padre.padre.a.lista_hermanos():
                self.comboBox1.Append(aux)
        else:
            for aux in self.parent.padre.padre.a.lista_hijos():
                self.comboBox1.Append(aux)

    def OnAnadirEnter(self, event):
        self.textCtrl2.SetFocus()

    def OnEdadEnter(self, event):
        self.bitmapButton1.SetFocus()

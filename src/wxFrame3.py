#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Piscos is a system information for psychologists.

(C) Copyright 2003-2014 Francisco Jose Moreno Llorca <packo@assamita.net>

Piscos is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Piscos is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Piscos.  If not, see <http://www.gnu.org/licenses/>.
"""
#Boa:Frame:wxFrame3
#Ficha personal

from wxPython.wx import *
from wxPython.stc import *

def create(parent):
    return wxFrame3(parent)

[wxID_WXFRAME3, wxID_WXFRAME3BUTTON1, wxID_WXFRAME3B_ANADIR_HERMANO, 
 wxID_WXFRAME3B_ANADIR_HIJO, wxID_WXFRAME3E_ANTES, wxID_WXFRAME3E_APELLIDOS, 
 wxID_WXFRAME3E_CODIGO, wxID_WXFRAME3E_CONYUGE, wxID_WXFRAME3E_DEMANDA, 
 wxID_WXFRAME3E_DEMANDA2, wxID_WXFRAME3E_DOMICILIO, wxID_WXFRAME3E_EDAD, 
 wxID_WXFRAME3E_EMPLEO, wxID_WXFRAME3E_ESTADO, wxID_WXFRAME3E_FAMILIA, 
 wxID_WXFRAME3E_HERMANOS, wxID_WXFRAME3E_HIJOS, wxID_WXFRAME3E_MADRE, 
 wxID_WXFRAME3E_MADRE_EDAD, wxID_WXFRAME3E_MECONOCIO, 
 wxID_WXFRAME3E_MEDICACION, wxID_WXFRAME3E_NOMBRE, wxID_WXFRAME3E_PADRE, 
 wxID_WXFRAME3E_PADRE_EDAD, wxID_WXFRAME3E_TLF1, wxID_WXFRAME3E_TLF2, 
 wxID_WXFRAME3T_APELLIDOS, wxID_WXFRAME3T_CODIGO, wxID_WXFRAME3T_COMENTARIOS, 
 wxID_WXFRAME3T_CONYUGE, wxID_WXFRAME3T_DOMICILIO, wxID_WXFRAME3T_EDAD, 
 wxID_WXFRAME3T_EMPLEO, wxID_WXFRAME3T_ESTADO, wxID_WXFRAME3T_HERMANOS, 
 wxID_WXFRAME3T_HIJOS, wxID_WXFRAME3T_IDANTES, wxID_WXFRAME3T_MADRE, 
 wxID_WXFRAME3T_MADRE_EDAD, wxID_WXFRAME3T_MECONOCIO, 
 wxID_WXFRAME3T_MEDICACION, wxID_WXFRAME3T_NOMBRE, wxID_WXFRAME3T_PADRE, 
 wxID_WXFRAME3T_PADRE_EDAD, wxID_WXFRAME3T_TELEF, 
] = map(lambda _init_ctrls: wxNewId(), range(45))

class wxFrame3(wxFrame):
    hijo = '0'  # utilizado para determinar que se esta tomando si los hijos o los hermanos
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wxFrame.__init__(self, id=wxID_WXFRAME3, name='', parent=prnt,
              pos=wxPoint(367, 122), size=wxSize(871, 593),
              style=wxDEFAULT_FRAME_STYLE, title='Error')
        self.SetClientSize(wxSize(863, 559))
        EVT_CLOSE(self, self.OnBotonSalir)

        self.t_nombre = wxStaticText(id=wxID_WXFRAME3T_NOMBRE, label='Name',
              name='t_nombre', parent=self, pos=wxPoint(48, 40), size=wxSize(56,
              13), style=0)

        self.t_apellidos = wxStaticText(id=wxID_WXFRAME3T_APELLIDOS,
              label='Surname', name='t_apellidos', parent=self,
              pos=wxPoint(48, 72), size=wxSize(64, 16), style=0)

        self.e_nombre = wxTextCtrl(id=wxID_WXFRAME3E_NOMBRE, name='e_nombre',
              parent=self, pos=wxPoint(136, 32), size=wxSize(192, 24),
              style=wxTE_PROCESS_ENTER, value='')
        EVT_TEXT_ENTER(self.e_nombre, wxID_WXFRAME3E_NOMBRE,
              self.OnE_nombreTextEnter)

        self.e_apellidos = wxTextCtrl(id=wxID_WXFRAME3E_APELLIDOS,
              name='e_apellidos', parent=self, pos=wxPoint(136, 64),
              size=wxSize(192, 24), style=wxTE_PROCESS_ENTER, value='')
        EVT_TEXT_ENTER(self.e_apellidos, wxID_WXFRAME3E_APELLIDOS,
              self.OnE_apellidosTextEnter)

        self.t_telef = wxStaticText(id=wxID_WXFRAME3T_TELEF, label='Phones',
              name='t_telef', parent=self, pos=wxPoint(336, 40), size=wxSize(60,
              13), style=0)

        self.e_tlf1 = wxTextCtrl(id=wxID_WXFRAME3E_TLF1, name='e_tlf1',
              parent=self, pos=wxPoint(424, 32), size=wxSize(80, 24),
              style=wxTE_PROCESS_ENTER, value='')
        EVT_TEXT_ENTER(self.e_tlf1, wxID_WXFRAME3E_TLF1, self.OnE_tlf1TextEnter)

        self.e_tlf2 = wxTextCtrl(id=wxID_WXFRAME3E_TLF2, name='e_tlf2',
              parent=self, pos=wxPoint(424, 64), size=wxSize(80, 24),
              style=wxTE_PROCESS_ENTER, value='')
        EVT_TEXT_ENTER(self.e_tlf2, wxID_WXFRAME3E_TLF2, self.OnE_tlf2TextEnter)

        self.t_codigo = wxStaticText(id=wxID_WXFRAME3T_CODIGO, label='Code',
              name='t_codigo', parent=self, pos=wxPoint(48, 112),
              size=wxSize(48, 16), style=0)
        self.t_codigo.SetToolTipString('Codigo')

        self.e_codigo = wxTextCtrl(id=wxID_WXFRAME3E_CODIGO, name='e_codigo',
              parent=self, pos=wxPoint(136, 104), size=wxSize(192, 24), style=0,
              value='')

        self.t_edad = wxStaticText(id=wxID_WXFRAME3T_EDAD, label='Age',
              name='t_edad', parent=self, pos=wxPoint(552, 40), size=wxSize(27,
              14), style=0)

        self.e_edad = wxTextCtrl(id=wxID_WXFRAME3E_EDAD, name='e_edad',
              parent=self, pos=wxPoint(608, 32), size=wxSize(40, 24),
              style=wxTE_PROCESS_ENTER, value='')
        EVT_TEXT_ENTER(self.e_edad, wxID_WXFRAME3E_EDAD, self.OnE_edadTextEnter)

        self.t_domicilio = wxStaticText(id=wxID_WXFRAME3T_DOMICILIO,
              label='Address', name='t_domicilio', parent=self,
              pos=wxPoint(352, 112), size=wxSize(50, 14), style=0)

        self.e_domicilio = wxTextCtrl(id=wxID_WXFRAME3E_DOMICILIO,
              name='e_domicilio', parent=self, pos=wxPoint(424, 104),
              size=wxSize(416, 24), style=wxTE_PROCESS_ENTER, value='')
        EVT_TEXT_ENTER(self.e_domicilio, wxID_WXFRAME3E_DOMICILIO,
              self.OnE_domicilioTextEnter)

        self.t_estado = wxStaticText(id=wxID_WXFRAME3T_ESTADO,
              label='Married?', name='t_estado', parent=self,
              pos=wxPoint(680, 40), size=wxSize(54, 14), style=0)

        self.t_empleo = wxStaticText(id=wxID_WXFRAME3T_EMPLEO, label='Job',
              name='t_empleo', parent=self, pos=wxPoint(552, 80),
              size=wxSize(41, 14), style=0)

        self.e_estado = wxTextCtrl(id=wxID_WXFRAME3E_ESTADO, name='e_estado',
              parent=self, pos=wxPoint(744, 32), size=wxSize(96, 24),
              style=wxTE_PROCESS_ENTER, value='')
        EVT_TEXT_ENTER(self.e_estado, wxID_WXFRAME3E_ESTADO,
              self.OnE_estadoTextEnter)

        self.e_empleo = wxTextCtrl(id=wxID_WXFRAME3E_EMPLEO, name='e_empleo',
              parent=self, pos=wxPoint(624, 72), size=wxSize(216, 22),
              style=wxTE_PROCESS_ENTER, value='')
        EVT_TEXT_ENTER(self.e_empleo, wxID_WXFRAME3E_EMPLEO,
              self.OnE_empleoTextEnter)

        self.t_idantes = wxStaticText(id=wxID_WXFRAME3T_IDANTES,
              label='Did you receive therapy before?:', name='t_idantes',
              parent=self, pos=wxPoint(48, 152), size=wxSize(173, 14), style=0)

        self.t_medicacion = wxStaticText(id=wxID_WXFRAME3T_MEDICACION,
              label='Medication', name='t_medicacion', parent=self,
              pos=wxPoint(424, 152), size=wxSize(97, 14), style=0)

        self.e_medicacion = wxTextCtrl(id=wxID_WXFRAME3E_MEDICACION,
              name='e_medicacion', parent=self, pos=wxPoint(544, 144),
              size=wxSize(296, 24), style=wxTE_PROCESS_ENTER, value='')
        EVT_TEXT_ENTER(self.e_medicacion, wxID_WXFRAME3E_MEDICACION,
              self.OnE_medicacionTextEnter)

        self.e_antes = wxCheckBox(id=wxID_WXFRAME3E_ANTES,
              label='Yes I did', name='e_antes', parent=self,
              pos=wxPoint(248, 144), size=wxSize(82, 37), style=0)
        self.e_antes.SetValue(False)

        self.e_familia = wxStaticBox(id=wxID_WXFRAME3E_FAMILIA, label='Family',
              name='e_familia', parent=self, pos=wxPoint(16, 312),
              size=wxSize(832, 152), style=0)

        self.e_demanda = wxStaticBox(id=wxID_WXFRAME3E_DEMANDA,
              label='Opening subject:', name='e_demanda',
              parent=self, pos=wxPoint(16, 184), size=wxSize(832, 128),
              style=0)

        self.e_demanda2 = wxStyledTextCtrl(id=wxID_WXFRAME3E_DEMANDA2,
              name='e_demanda2', parent=self, pos=wxPoint(24, 208),
              size=wxSize(816, 96), style=0)
        self.e_demanda2.SetUseHorizontalScrollBar(False)
        self.e_demanda2.SetMarginLeft(0)

        self.t_conyuge = wxStaticText(id=wxID_WXFRAME3T_CONYUGE,
              label='Couple: ', name='t_conyuge', parent=self, pos=wxPoint(48,
              336), size=wxSize(58, 14), style=0)

        self.e_conyuge = wxTextCtrl(id=wxID_WXFRAME3E_CONYUGE, name='e_conyuge',
              parent=self, pos=wxPoint(112, 328), size=wxSize(584, 24),
              style=wxTE_PROCESS_ENTER, value='')
        EVT_TEXT_ENTER(self.e_conyuge, wxID_WXFRAME3E_CONYUGE,
              self.OnE_conyugeTextEnter)

        self.t_comentarios = wxStaticText(id=wxID_WXFRAME3T_COMENTARIOS,
              label='(name-comments)', name='t_comentarios', parent=self,
              pos=wxPoint(704, 336), size=wxSize(136, 16), style=0)

        self.e_hijos = wxListBox(choices=[], id=wxID_WXFRAME3E_HIJOS,
              name='e_hijos', parent=self, pos=wxPoint(88, 368),
              size=wxSize(136, 88), style=0, validator=wxDefaultValidator)
        self.e_hijos.SetLabel('Children')

        self.t_hijos = wxStaticText(id=wxID_WXFRAME3T_HIJOS, label='Children:',
              name='t_hijos', parent=self, pos=wxPoint(40, 376), size=wxSize(32,
              14), style=0)

        self.t_padre = wxStaticText(id=wxID_WXFRAME3T_PADRE, label='Father',
              name='t_padre', parent=self, pos=wxPoint(240, 376),
              size=wxSize(48, 14), style=0)

        self.t_madre = wxStaticText(id=wxID_WXFRAME3T_MADRE, label='Mother',
              name='t_madre', parent=self, pos=wxPoint(240, 408),
              size=wxSize(40, 14), style=0)

        self.e_padre = wxTextCtrl(id=wxID_WXFRAME3E_PADRE, name='e_padre',
              parent=self, pos=wxPoint(288, 368), size=wxSize(152, 24),
              style=wxTE_PROCESS_ENTER, value='')
        EVT_TEXT_ENTER(self.e_padre, wxID_WXFRAME3E_PADRE,
              self.OnE_padreTextEnter)

        self.e_madre = wxTextCtrl(id=wxID_WXFRAME3E_MADRE, name='e_madre',
              parent=self, pos=wxPoint(288, 400), size=wxSize(152, 24),
              style=wxTE_PROCESS_ENTER, value='')
        EVT_TEXT_ENTER(self.e_madre, wxID_WXFRAME3E_MADRE,
              self.OnE_madreTextEnter)

        self.t_hermanos = wxStaticText(id=wxID_WXFRAME3T_HERMANOS,
              label='Brothers:', name='t_hermanos', parent=self,
              pos=wxPoint(608, 376), size=wxSize(56, 14), style=0)

        self.e_hermanos = wxListBox(choices=[], id=wxID_WXFRAME3E_HERMANOS,
              name='e_hermanos', parent=self, pos=wxPoint(672, 368),
              size=wxSize(160, 72), style=0, validator=wxDefaultValidator)

        self.t_meconocio = wxStaticText(id=wxID_WXFRAME3T_MECONOCIO,
              label='Link', name='t_meconocio', parent=self,
              pos=wxPoint(24, 488), size=wxSize(73, 16), style=0)

        self.e_meconocio = wxTextCtrl(id=wxID_WXFRAME3E_MECONOCIO,
              name='e_meconocio', parent=self, pos=wxPoint(112, 480),
              size=wxSize(736, 22), style=wxTE_PROCESS_ENTER, value='')

        self.e_padre_edad = wxTextCtrl(id=wxID_WXFRAME3E_PADRE_EDAD,
              name='e_padre_edad', parent=self, pos=wxPoint(456, 368),
              size=wxSize(40, 24), style=wxTE_PROCESS_ENTER, value='')
        EVT_TEXT_ENTER(self.e_padre_edad, wxID_WXFRAME3E_PADRE_EDAD,
              self.OnE_padre_edadTextEnter)

        self.e_madre_edad = wxTextCtrl(id=wxID_WXFRAME3E_MADRE_EDAD,
              name='e_madre_edad', parent=self, pos=wxPoint(456, 400),
              size=wxSize(40, 24), style=wxTE_PROCESS_ENTER, value='')
        EVT_TEXT_ENTER(self.e_madre_edad, wxID_WXFRAME3E_MADRE_EDAD,
              self.OnE_madre_edadTextEnter)

        self.t_padre_edad = wxStaticText(id=wxID_WXFRAME3T_PADRE_EDAD,
              label='age', name='t_padre_edad', parent=self,
              pos=wxPoint(504, 376), size=wxSize(27, 14), style=0)

        self.t_madre_edad = wxStaticText(id=wxID_WXFRAME3T_MADRE_EDAD,
              label='age', name='t_madre_edad', parent=self,
              pos=wxPoint(504, 408), size=wxSize(27, 14), style=0)

        self.b_anadir_hijo = wxBitmapButton(bitmap=wxNullBitmap,
              id=wxID_WXFRAME3B_ANADIR_HIJO, name='b_anadir_hijo', parent=self,
              pos=wxPoint(32, 400), size=wxSize(42, 34), style=wxBU_AUTODRAW,
              validator=wxDefaultValidator)
        self.b_anadir_hijo.SetBitmapDisabled(wxBitmap('edit_add.png',
              wxBITMAP_TYPE_PNG))
        self.b_anadir_hijo.SetBitmapSelected(wxBitmap('edit_add.png',
              wxBITMAP_TYPE_PNG))
        self.b_anadir_hijo.SetBitmapFocus(wxBitmap('edit_add.png',
              wxBITMAP_TYPE_PNG))
        self.b_anadir_hijo.SetBitmapLabel(wxBitmap('edit_add.png',
              wxBITMAP_TYPE_PNG))
        EVT_BUTTON(self.b_anadir_hijo, wxID_WXFRAME3B_ANADIR_HIJO,
              self.OnB_anadir_hijoButton)

        self.b_anadir_hermano = wxBitmapButton(bitmap=wxNullBitmap,
              id=wxID_WXFRAME3B_ANADIR_HERMANO, name='b_anadir_hermano',
              parent=self, pos=wxPoint(616, 400), size=wxSize(40, 32),
              style=wxBU_AUTODRAW, validator=wxDefaultValidator)
        self.b_anadir_hermano.SetBitmapDisabled(wxBitmap('edit_add.png',
              wxBITMAP_TYPE_PNG))
        self.b_anadir_hermano.SetBitmapFocus(wxBitmap('edit_add.png',
              wxBITMAP_TYPE_PNG))
        self.b_anadir_hermano.SetBitmapLabel(wxBitmap('edit_add.png',
              wxBITMAP_TYPE_PNG))
        self.b_anadir_hermano.SetBitmapSelected(wxBitmap('edit_add.png',
              wxBITMAP_TYPE_PNG))
        EVT_BUTTON(self.b_anadir_hermano, wxID_WXFRAME3B_ANADIR_HERMANO,
              self.OnB_anadir_hermanoButton)

        self.button1 = wxButton(id=wxID_WXFRAME3BUTTON1,
              label='Save and exit', name='button1', parent=self,
              pos=wxPoint(352, 520), size=wxSize(200, 24), style=0)
        self.button1.SetBackgroundColour(wxColour(128, 128, 128))
        EVT_BUTTON(self.button1, wxID_WXFRAME3BUTTON1, self.OnBotonSalir)

    def __init__(self, parent):
        self._init_ctrls(parent)

    def OnBotonSalir(self, event):
        self.guardar_elementos()
        self.padre.padre.mostrar_datos(self.datos['nombre'],self.datos['apellidos'])
        self.padre.Show()
        self.Destroy()

    def mostrar_elementos(self):
        self.e_hermanos.Clear()
        self.e_hijos.Clear()
        self.e_nombre.SetValue(self.datos['nombre'])
        self.e_codigo.SetValue(self.datos['codigo'])
        if self.datos['tlf1']!= '0':
            self.e_tlf1.SetValue(self.datos['tlf1'])
        if self.datos['tlf2']!= '0':
            self.e_tlf2.SetValue(self.datos['tlf2'])
        if self.datos['domicilio']!= '0':    
            self.e_domicilio.SetValue(self.datos['domicilio'])
        if self.datos['edad']!= '0':
            self.e_edad.SetValue(self.datos['edad'])
        if self.datos['empleo']!= '0':
            self.e_empleo.SetValue(self.datos['empleo'])
        if self.datos['estado']!= '0':
            self.e_estado.SetValue(self.datos['estado'])
        if self.datos['apellidos']!= '0':
            self.e_apellidos.SetValue(self.datos['apellidos'])
        if self.datos['conyuge']!= '0':
            self.e_conyuge.SetValue(self.datos['conyuge'])
        if self.datos['padre']!= '0':
            self.e_padre.SetValue(self.datos['padre'])
        if self.datos['padre_edad']!= '0':
            self.e_padre_edad.SetValue(self.datos['padre_edad'])
        if self.datos['madre']!= '0':
            self.e_madre.SetValue(self.datos['madre'])
        if self.datos['madre_edad']!= '0':
            self.e_madre_edad.SetValue(self.datos['madre_edad'])
        if self.datos['hijos'] == 'Si':
            for aux in self.padre.padre.a.lista_hijos():
                self.e_hijos.Append(aux)
        if self.datos['hermanos'] == 'Si':
            for aux in self.padre.padre.a.lista_hermanos():
                self.e_hermanos.Append(aux)
    def guardar_elementos(self):
        self.datos['nombre'] = self.e_nombre.GetValue()
        self.datos['codigo'] = self.e_codigo.GetValue()
        self.datos['edad'] = self.e_edad.GetValue()
        self.datos['tlf1'] = self.e_tlf1.GetValue()
        self.datos['tlf2'] = self.e_tlf2.GetValue()
        self.datos['domicilio'] = self.e_domicilio.GetValue()
        self.datos['conyuge'] = self.e_conyuge.GetValue()
        self.datos['estado'] = self.e_estado.GetValue()
        self.datos['empleo'] = self.e_empleo.GetValue()
        #self.datos['hijos'] = self.e_hijos.GetValue()
        self.datos['padre'] = self.e_padre.GetValue()
        self.datos['padre_edad'] = self.e_padre_edad.GetValue()
        self.datos['madre'] = self.e_madre.GetValue()
        self.datos['madre_edad'] = self.e_madre_edad.GetValue()
        #self.datos['hermanos'] = self.e_hermanos.GetValue()
        self.datos['apellidos'] = self.e_apellidos.GetValue()
        self.padre.padre.a.modificar()

    def OnE_apellidosTextEnter(self, event):
        self.e_tlf1.SetFocus()

    def OnE_tlf1TextEnter(self, event):
        self.e_tlf2.SetFocus()

    def OnE_tlf2TextEnter(self, event):
        self.e_edad.SetFocus()

    def OnE_edadTextEnter(self, event):
        self.e_domicilio.SetFocus()

    def OnE_domicilioTextEnter(self, event):
        self.e_estado.SetFocus()

    def OnE_estadoTextEnter(self, event):
        self.e_empleo.SetFocus()

    def OnE_empleoTextEnter(self, event):
        self.e_medicacion.SetFocus()

    def OnE_medicacionTextEnter(self, event):
        self.e_conyuge.SetFocus()

    def OnE_conyugeTextEnter(self, event):
        self.e_padre.SetFocus()

    def OnE_padreTextEnter(self, event):
        self.e_padre_edad.SetFocus()

    def OnE_madreTextEnter(self, event):
        self.e_madre_edad.SetFocus()

    def OnE_padre_edadTextEnter(self, event):
        self.e_madre.SetFocus()

    def OnE_madre_edadTextEnter(self, event):
        self.e_nombre.SetFocus()

    def OnE_nombreTextEnter(self, event):
        self.e_apellidos.SetFocus()

    def OnB_anadir_hijoButton(self, event):
        self.hijo = '1'
        import wxMiniFrame1
        self.hijos = wxMiniFrame1.create(self)
        self.hijos.conexion = self.padre.padre.a
        self.hijos.SetTitle('Children')
        self.hijos.Show()

    def OnB_anadir_hermanoButton(self, event):
        self.hijo = '0'
        import wxMiniFrame1
        self.hermanos = wxMiniFrame1.create(self)
        self.hermanos.conexion = self.padre.padre.a
        self.hermanos.SetTitle('Brother and sister')
        self.hermanos.Show()

        

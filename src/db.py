#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Piscos is a system information for psychologists.

(C) Copyright 2003 Francisco Jose Moreno Llorca <packo@assamita.net>

Piscos is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Piscos is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Piscos.  If not, see <http://www.gnu.org/licenses/>.
"""
import MySQLdb
from random import *

HOST='localhost'
USER='user'
PASSWD='passwd'
DB='db'

class dbas:

    datos = { 'nombre':'0','codigo':'0','edad':'0','tlf1':'0','tlf2':'0','domicilio':'0','conyuge':'0','estado':'0','empleo':'0','hijos':'No','padre':'0','padre_edad':'0','madre':'0','madre_edad':'0','hermanos':'No','apellidos':'0'}
    datos_orden = 'codigo','nombre','edad','tlf1','tlf2','domicilio','conyuge','estado','empleo','hijos','padre','padre_edad','madre','madre_edad','hermanos','apellidos'
    hermano = { 'nombre': '0','edad':'0','tipo':'0'}
    hermano_orden = 'nombre','edad','tipo'
    hijos = []
    hermanos = []

    def __init__(self):
        self.dbase = MySQLdb.connect(host=HOST,user=USER,passwd=PASSWD,db=DB)
        
    def pido_nombre(self,nombre,apellidos):
        if apellidos == "":
            orden = r"SELECT * FROM pacientes where nombre = '" + nombre + "'"
        else:
            orden = r"SELECT * FROM pacientes where (nombre = '" + nombre + "' AND '"+apellidos+"' LIKE apellidos)"
        self.dbase.query (orden)
        salida = self.dbase.use_result()
        a = salida.fetch_row()
        indice = 0
        for guardado in self.datos_orden:
            self.datos[guardado] = a[0][indice]
            indice = indice +1
    
    
    def pido_codigo(self,codigo):
        #TODO: finish
        orden = r"SELECT * FROM pacientes where codigo = '" + codigo + "';" 
        self.dbase.query (orden)
        salida = self.dbase.use_result()
        a = salida.fetch_row()
        indice = 0
        for guardado in self.datos_orden:
            self.datos[guardado] = a[0][indice]
            indice = indice +1
    
    def nuevo_hermano(self):
        a = '(\'' + self.hermano['nombre']+ '\'' + ','
        a = a + '\'' + self.hermano['edad']+ '\'' + ','
        a = a + '\'' + self.hermano['tipo']+ '\'' + ')'
        orden = "INSERT INTO " + self.datos['codigo'] + " (nombre,edad,tipo) VALUES " + a
        self.dbase.query (orden)
        
    def nuevo(self):
        a = '(\'' + self.datos['nombre'] + '\'' + ','
        a = a +'\'' +  self.datos['codigo'] +'\'' +  ','
        a = a +'\'' +  self.datos['edad'] +'\'' +  ','
        a = a +'\'' +  self.datos['tlf1'] +'\'' +  ','
        a = a +'\'' +  self.datos['tlf2'] +'\'' +  ','
        a = a +'\'' +  self.datos['domicilio'] +'\'' +  ','
        a = a + '\'' + self.datos['conyuge'] +'\'' +  ','
        a = a + '\'' + self.datos['estado'] +'\'' +  ','
        a = a + '\'' + self.datos['empleo'] +'\'' +  ','
        a = a + '\'' + self.datos['hijos'] +'\'' +  ','
        a = a + '\'' + self.datos['padre'] +'\'' +  ','
        a = a + '\'' + self.datos['padre_edad'] +'\'' +  ','
        a = a + '\'' + self.datos['madre'] +'\'' +  ','
        a = a + '\'' + self.datos['madre_edad'] +'\'' +  ','
        a = a + '\'' + self.datos['hermanos'] +'\'' + ','
        a = a + '\'' + self.datos['apellidos'] +'\'' + ')'
        
        orden = "INSERT INTO pacientes (nombre,codigo,edad,tlf1,tlf2,domicilio,conyuge,estado,empleo,hijos,padre,padre_edad,madre,madre_edad,hermanos,apellidos) VALUES " + a
        self.dbase.query (orden)

    def crear(self,tabla):
        if tabla == "pacientes":
            self.dbase.query ( """
                CREATE TABLE pacientes
                (
                    codigo CHAR(10) not null primary key,
                    nombre CHAR(40) not null,
                    edad CHAR(2),
                    tlf1 CHAR(9),
                    tlf2 CHAR(9),
                    domicilio CHAR(100),
                    conyuge CHAR(40),
                    estado CHAR(15),
                    empleo CHAR(40),
                    hijos CHAR(2),
                    padre CHAR(40),
                    padre_edad CHAR(2),
                    madre CHAR(40),
                    madre_edad CHAR(2),
                    hermanos CHAR(2),      
                    apellidos CHAR(40)        
                )
                """)
        elif tabla == "hermanos":
        
        #    La tabla de hermanos/hijos va de la siguiente manera:
        #    Se meten los datos de nombre y edad y:
        #        -0 si es hermano
        #        -1 si es hijo
        
            nombre = self.datos['codigo']
            orden = " CREATE  TABLE " + nombre + " ( nombre CHAR(40), edad CHAR(2),tipo CHAR(1))"
            self.dbase.query(orden)
    
    def modificar(self):
    #OJO que borro la entrada del paciente entera
        orden = "DELETE FROM pacientes WHERE codigo='"+ self.datos['codigo']+ "'"
        self.dbase.query(orden)
        self.nuevo()
    
    def eliminar_hermano(self):
        orden = "DELETE FROM " + self.datos['codigo'] + " WHERE nombre = '" + self.hermano['nombre'] + "'"
        self.dbase.query(orden)
        
    def eliminar_tabla(self,tabla):
        self.dbase.query("DROP TABLE " + tabla + ";")
    
    def lista_nombres(self):
        self.dbase.query("SELECT nombre,apellidos FROM pacientes")
        salida = self.dbase.store_result()
        listado_nombres = []
        resultado =[]
        nulo = ()
        while 1: 
            listado_nombres = salida.fetch_row()
            if listado_nombres == nulo :
                break
            resultado.append(listado_nombres[0][0] +" "+listado_nombres[0][1] )
        return resultado
    
    def lista_hermanos(self):
        if self.datos['hermanos'] == 'Si':
            orden = "SELECT nombre,edad FROM "+ self.datos['codigo']+ " WHERE tipo='0'"
            self.dbase.query(orden)
            salida = self.dbase.store_result()
            listado_nombres = []
            resultado =[]
            nulo = ()
            while 1: 
                listado_nombres = salida.fetch_row()
                if listado_nombres == nulo :
                    break
                resultado.append(listado_nombres[0][0] + " edad: " + listado_nombres[0][1])
            return resultado
        else:
            return []            
    
    def lista_hijos(self):
        if self.datos['hijos'] == 'Si':
            orden = "SELECT nombre,edad FROM "+ self.datos['codigo']+ " WHERE tipo='1'"
            self.dbase.query(orden)
            salida = self.dbase.store_result()
            listado_nombres = []
            resultado =[]
            nulo = ()
            while 1: 
                listado_nombres = salida.fetch_row()
                if listado_nombres == nulo :
                    break
                resultado.append(listado_nombres[0][0]+ " edad: " + listado_nombres[0][1])
            return resultado
        else:
            return []
    def genera_codigo(self):
        orden = "SELECT codigo FROM pacientes"
        self.dbase.query(orden)
        salida = self.dbase.use_result()
        a = salida.fetch_row()
        while 1:
            numero = randrange(1000,99999)
            cadena = 'a' +str(numero)
            if a == []:
                return cadena
            if cadena not in a:
                return cadena
        
    def elimina_paciente(self,codigo):
        orden = "DELETE FROM pacientes WHERE codigo='"+ codigo +"' "
        self.dbase.query(orden)
        try:
            self.eliminar_tabla(codigo)
        except:
            print "No se han borrado los hermanos/hijos"
        
    #def salir(self):
        #self.dbase.commit()
        #self.dbase.close()
        
        
if __name__ == '__main__':
    a = dbas()
    #a.eliminar_tabla("pacientes")
    a.crear('pacientes')
    # for testing:
    #a.datos['nombre'] = 'Ramiro'
    #a.datos['codigo'] = 'a22239'
    #a.nuevo()
    #a.datos['nombre'] = 'Pepito'
    #a.datos['codigo'] = 'a22234'
    #a.nuevo()
    a.dbase.query("show tables")
    #print a.dbase.use_result()
    #print a.lista_nombres()

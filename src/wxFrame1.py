#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Piscos is a system information for psychologists.

(C) Copyright 2003 Francisco Jose Moreno Llorca <packo@assamita.net>

Piscos is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Piscos is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Piscos.  If not, see <http://www.gnu.org/licenses/>.
"""
#Boa:Frame:wxFrame1
#Inicio del programa

from wxPython.wx import *
import string
from db import dbas
from psico import datapsico

def create(parent):
    return wxFrame1(parent)

[wxID_WXFRAME1, wxID_WXFRAME1BUTTON1, wxID_WXFRAME1COMBOBOX1, 
 wxID_WXFRAME1STATICBITMAP1, wxID_WXFRAME1STATICTEXT1, wxID_WXFRAME1TEXTCTRL1, 
] = map(lambda _init_ctrls: wxNewId(), range(6))



class wxFrame1(wxFrame):
    listau = []
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wxFrame.__init__(self, id=wxID_WXFRAME1, name='', parent=prnt,
              pos=wxPoint(441, 338), size=wxSize(419, 114),
              style=wxDEFAULT_FRAME_STYLE, title='Piscos - Select user')
        self.SetClientSize(wxSize(419, 114))
        self.SetHelpText('Choose an user and click "Next"')
        self.SetBackgroundColour(wxColour(95, 159, 159))
        self.SetToolTipString('Choose an user and click "Next"')
        EVT_CLOSE(self, self.OnWxframe1Close)

        self.textCtrl1 = wxTextCtrl(id=wxID_WXFRAME1TEXTCTRL1, name='textCtrl1',
              parent=self, pos=wxPoint(72, 16), size=wxSize(88, 24),
              style=wxTE_PROCESS_ENTER, value='')
        self.textCtrl1.SetToolTipString('Fill with the given name, it will open if it\'s unique')
        EVT_TEXT_ENTER(self.textCtrl1, wxID_WXFRAME1TEXTCTRL1,
              self.OnEntradaEnter)

        self.staticText1 = wxStaticText(id=wxID_WXFRAME1STATICTEXT1,
              label='Name', name='staticText1', parent=self, pos=wxPoint(24,
              24), size=wxSize(43, 16), style=0)

        self.comboBox1 = wxComboBox(choices=self.listau, id=wxID_WXFRAME1COMBOBOX1,
              name='comboBox1', parent=self, pos=wxPoint(176, 16),
              size=wxSize(240, 24), style=0, validator=wxDefaultValidator,
              value='New')
        self.comboBox1.SetLabel('')
        self.comboBox1.SetHelpText('Choose an user and click "Next"')
        self.comboBox1.SetToolTipString('Choose an user and click "Next", choose "New" if you want to register a new one')

        self.button1 = wxButton(id=wxID_WXFRAME1BUTTON1, label='Next',
              name='button1', parent=self, pos=wxPoint(48, 72), size=wxSize(104,
              24), style=0)
        self.button1.SetToolTipString('Click here if the user is chosen')
        EVT_BUTTON(self.button1, wxID_WXFRAME1BUTTON1, self.OnButton1Button)

        self.staticBitmap1 = wxStaticBitmap(bitmap=wxBitmap('icon.bmp',
              wxBITMAP_TYPE_BMP), id=wxID_WXFRAME1STATICBITMAP1,
              name='staticBitmap1', parent=self, pos=wxPoint(216, 40),
              size=wxSize(112, 72), style=0)

    def __init__(self, parent):
        self.a= dbas()
        self.listau = self.a.lista_nombres()
        self.listau.append('New');
        
        self._init_ctrls(parent)
        self.textCtrl1.SetFocus()
        
        

    def nombre_metido(self, event):
        event.Skip()

    def OnButton1Button(self, event):
        paciente = self.textCtrl1.GetValue()
        if paciente in self.listau:
            res = self.textCtrl1.GetValue().split()
            try:                
                paciente = res[0]
                apellido = res[1]
            except:                
                paciente = res[0]
                apellido =""
            print paciente + apellido
            self.abrir_paciente(paciente,apellido)
            
        elif paciente == 'New':
            self.si_nuevo()
            
        else:
            paciente = self.comboBox1.GetValue()
            if paciente == 'New':
                print "A new one"
                self.si_nuevo()
            elif paciente in self.listau:
                res = self.comboBox1.GetValue().split()
                try:
                    paciente = res[0]
                    apellido = res[1]
                except:
                    paciente = res[0]
                    apellido =""
                self.abrir_paciente(paciente,apellido)

    def abrir_paciente(self,paciente, apellido=""):
        #paciente = self.textCtrl1.GetValue()
        self.Hide()
        import wxFrame2
        self.usuario = wxFrame2.create(None)
        self.usuario.paciente = paciente
        self.usuario.padre = self
        
        
        self.mostrar_datos(paciente,apellido)
        # needed when running from Boa under Windows 9X
        #self.SetTopWindow(self.paciente)
        
        #incicializar datos psicologicos
        self.usuario.textos = datapsico(self)
        self.usuario.textos.abrir_paciente_notas(self.a.datos['codigo'])
        self.usuario.textos.abrir_paciente_sesiones(self.a.datos['codigo'])
        
        self.usuario.Show()
        
    def mostrar_datos(self,paciente,apellidos):
        self.a.pido_nombre(paciente,apellidos)
        self.usuario.SetTitle(paciente)
        self.usuario.textCtrl1.SetValue(self.a.datos['nombre'])
        self.usuario.textCtrl1.SetEditable(0)
        if self.a.datos['tlf1'] != '0':
            self.usuario.textCtrl2.SetValue(self.a.datos['tlf1'])
        self.usuario.textCtrl2.SetEditable(0)
        if self.a.datos['tlf2'] != '0':
            self.usuario.textCtrl3.SetValue(self.a.datos['tlf2'])
        self.usuario.textCtrl3.SetEditable(0)
        #datos psicologicos:
        
             

    def OnWxframe1Close(self, event):
        #NO FUNCIONA
        #self.a.salir()
        event.Skip()

    def OnEntradaEnter(self, event):
        paciente = self.textCtrl1.GetValue()
        if paciente in self.listau:
            res = self.textCtrl1.GetValue().split()
            try:                
                paciente = res[0]
                apellido = res[1]
            except:                
                paciente = res
            print paciente
            self.abrir_paciente(paciente,apellido)
            
        elif paciente == 'New':
            self.si_nuevo()
            
    def si_nuevo(self):
        self.a.datos['codigo'] = self.a.genera_codigo()
        self.a.datos['nombre'] = 'New'
        self.a.datos['edad'] = ""
        self.a.datos['tlf1'] = ""
        self.a.datos['tlf2'] = ""
        self.a.datos['domicilio'] = ""
        self.a.datos['conyuge'] = ""
        self.a.datos['estado'] = ""
        self.a.datos['empleo'] = ""
        self.a.datos['hijos'] = 'No'
        self.a.datos['padre'] = ""
        self.a.datos['padre_edad'] = ""
        self.a.datos['madre'] = ""
        self.a.datos['madre_edad'] = ""
        self.a.datos['hermanos'] = 'No'
        self.a.datos['apellidos'] = ""
        self.a.nuevo()
        
        self.abrir_paciente('New',"")
        

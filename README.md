piscos
======

Piscos is a system information for psychologists.

Graphical frontend with wxWindows and Mysql for storage.

You must initialize the db executing _python db.py_

TODO
-----
* Configuration files
* Finish the uncompleted parts
* Do packable
 

Authors
-------
Francisco José Moreno Llorca - packo@assamita.net - @kotejante

License
-------

	(C) Copyright 2003-2014 Francisco Jose Moreno Llorca <packo@assamita.net>

	Piscos is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Piscos is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Piscos.  If not, see <http://www.gnu.org/licenses/>.
